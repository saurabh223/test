import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from "react-bootstrap/esm/Container";
import { Cart,Person } from "react-bootstrap-icons/build";
import Login from "./Login";
import Profile from './Profile'

function Header(){
    return(
        <Router>
        <div>
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="#home">
                <img
                  alt=""
                  src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
                  width="30"
                  height="30"
                  className="d-inline-block align-top" 
                />{' '}
                Brand Name
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto text-center">
                  <Link to="/" className="menu"><Nav.Link href="#home" >Home</Nav.Link></Link>
                
                  <Link to="/contact" className="menu"><Nav.Link href="#contact">Contact Us</Nav.Link>
                  </Link>
                  <Link to="/Product" className="menu"><Nav.Link href="#products">Produts</Nav.Link>
                  </Link>
         
                  <input type='text' placeholder="Search" className="search" ></input>
                  
                </Nav>
              </Navbar.Collapse>
              <Link to="/Cart" className="me-2"><Cart size={26}  color="black"/></Link>
              <Link to="/login" className="User"><Person size={26} color="black"/>Log in  </Link>
            </Container>
          </Navbar>
          <Routes>
            
            <Route exact path='/login' element={<Login />}></Route>
            <Route exact path='/Profile' element={<Profile />}></Route>
          


          </Routes>
        </div>
      </Router>
    )
}
export default Header;