
import React from "react";
import { useNavigate } from "react-router-dom";
function Profile(props){
    const navigate=useNavigate();
    const handleLogout = () => {
        navigate('/login');
      }
    return(
        <div>
            <h1>Profile page!</h1>
            <input type="button" onClick={handleLogout} value="Logout" />
        </div>
    )
}
export default Profile;