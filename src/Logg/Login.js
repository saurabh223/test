import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';

function Login(props) {
  const [loading, setLoading] = useState(false);
  const username = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const navigate=useNavigate();

  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.post('https://reqres.in/api/login', { username: username.value, password: password.value }).then(response => {
      setLoading(false);
      navigate('/profile');
    }).catch(error => {
      setLoading(false);
      if (error.response.status === 401) setError(error.response.data.message);
      else setError("Something went wrong. Please try again later.");
    });
  }

  return (
    <div>
      <Form className='m-3'>
        <h1>Login Here</h1>
      <Form.Group className="mb-3 mt-2">
      <Form.Control type="text" {...username}  />
                </Form.Group>
                <Form.Group className="mb-3 mt-2">
                <Form.Control type="password" {...password}  />
                </Form.Group>
                {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <Button type="button"  onClick={handleLogin} >Login</Button>
      </Form>
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Login;




